<?php

class Database
{
    private $db;

    function __construct()
    {
        $db = parse_ini_file('database.ini');
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $dbh = new PDO('mysql:host=' . $db['DB_HOST'] . ';dbname=' . $db['DB_DATABASE'], $db['DB_USERNAME'], $db['DB_PASSWORD'], $opt);
        $this->db = $dbh;
    }

    /**
     * @return PDO
     */
    protected function getDb()
    {
        return $this->db;
    }

    protected function all($table)
    {
        $query = 'SELECT * FROM ' . $table;
        $records = [];
        $result = $this->db->query($query);
        foreach ($result as $row) {
            $records[$row['id']] = $row;
        }
        return $records;
    }

    protected function fetchOneByQuery($query, $args)
    {
        $stmt = $this->db->prepare($query);
        $stmt->execute($args);
        $result = $stmt->fetch();
        return $result;
    }

    protected function fetchAllByQuery($query, $args, $opt = '', $className='')
    {
        if (empty($args) || !is_array($args)) {
            return $this->fetchAll($query, $opt, $className);
        }
        $stmt = $this->db->prepare($query);
        $stmt->execute($args);
        if (!empty($className)) {
            $result = $stmt->fetchAll($opt, $className);
        } else {
            $result = $stmt->fetchAll($opt);
        }
        return $result;
    }

    protected function fetchAll($query, $opt, $className)
    {
        $stmt = $this->db->query($query);
        if (!empty($className)) {
            $result = $stmt->fetchAll($opt, $className);
        } else {
            $result = $stmt->fetchAll($opt);
        }
        return $result;
    }

    protected function fetchColumnByQuery($query, $args)
    {
        return $this->fetchAllByQuery($query, $args, PDO::FETCH_COLUMN);
    }

    protected function fetchPairByQuery($query, $args)
    {
        return $this->fetchAllByQuery($query, $args, PDO::FETCH_KEY_PAIR);
    }

    protected function fetchDistinctByQuery($query, $args)
    {
        return $this->fetchAllByQuery($query, $args, PDO::FETCH_UNIQUE);
    }

    protected function fetchObject($query, $args, $className)
    {
        return $this->fetchAllByQuery($query, $args, PDO::FETCH_CLASS, $className);
    }

    protected function insert($query, $args)
    {
        $stmt = $this->db->prepare($query);
        for($i=1; $i<=count($args); $i++) {
            $stmt->bindValue($i, $args[$i-1]);
        }
        $stmt->execute();
    }
}