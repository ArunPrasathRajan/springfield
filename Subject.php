<?php

require_once 'database.php';

class Subject extends Database
{
    const TABLE = 'subjects';

    public $id;

    public $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getSubjects()
    {
        $query = 'SELECT * FROM ' . self::TABLE;
        return $this->fetchObject($query, '', get_class($this));
    }

    public function getSubjectName($id)
    {
        $query = 'SELECT name from '. self::TABLE . ' WHERE id = ?';
        $args = [$id];
        return $this->fetchObject($query, $args, get_class($this))[0]->name;
    }

    public function getNumberOfSubjects()
    {
        $query = 'SELECT COUNT(*) as subjectCount FROM ' . self::TABLE;
        return $this->fetchObject($query, '', get_class($this))[0]->subjectCount;
    }
}