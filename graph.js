$(function(){
   
        var chart = new CanvasJS.Chart("chartContainer", {
            title: {
                text: "Basic Column Chart"
            },
            data: [{
                type: "column",
                dataPoints: [
                { y: 45, label: "Apple" },
                { y: 31, label: "Mango" },
                { y: 52, label: "Pineapple" },
                { y: 10, label: "Grapes" },
                { y: 46, label: "Lemon" },
                { y: 30, label: "Banana" },
                { y: 50, label: "Guava" },
                ]
            }]
        });
        chart.render();
   
});