<?php

require_once 'database.php';

class Student extends Database
{
    const TABLE = 'students';

    public $id;

    public $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getStudents()
    {
        $query = 'SELECT * FROM ' . self::TABLE;
        return $this->fetchObject($query, '', get_class($this));
    }

    public function getStudentName($id)
    {
        $query = 'SELECT name from '. self::TABLE . ' WHERE id = ?';
        $args = [$id];
        return $this->fetchObject($query, $args, get_class($this))[0]->name;
    }

    public function getNumberOfStudents()
    {
        $query = 'SELECT COUNT(*) as studentCount FROM ' . self::TABLE;
        return $this->fetchObject($query, '', get_class($this))[0]->studentCount;
    }
}