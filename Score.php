<?php

require_once 'database.php';

class Score extends Database
{
    const TABLE = 'scores';

    public $id;

    public $studentID;

    public $subjectID;

    public $testID;

    public $marks;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getMarks()
    {
        return $this->marks;
    }

    /**
     * @return mixed
     */
    public function getStudentID()
    {
        return $this->studentID;
    }

    /**
     * @return mixed
     */
    public function getSubjectID()
    {
        return $this->subjectID;
    }

    /**
     * @return mixed
     */
    public function getTestID()
    {
        return $this->testID;
    }

    /**
     * @param mixed $marks
     */
    public function setMarks($marks)
    {
        $this->marks = $marks;
    }

    /**
     * @param mixed $studentID
     */
    public function setStudentID($studentID)
    {
        $this->studentID = $studentID;
    }

    /**
     * @param mixed $subjectID
     */
    public function setSubjectID($subjectID)
    {
        $this->subjectID = $subjectID;
    }

    /**
     * @param mixed $testID
     */
    public function setTestID($testID)
    {
        $this->testID = $testID;
    }

    public function getScores()
    {
        $query = 'SELECT * FROM ' . self::TABLE;
        return $this->fetchObject($query, '', get_class($this));
    }


    public function insertScore($studentID, $subjectID, $testID, $marks)
    {
        $query = 'INSERT INTO ' . self::TABLE . '(studentID, subjectID, testID, marks) values (?,?,?,?)';
        $args = [$studentID, $subjectID, $testID, $marks];
        return $this->insert($query, $args);
    }

    public function getTotalMarksForStudentByTest($studentID, $testID)
    {
        $query = 'SELECT SUM(marks) FROM ' . self::TABLE . ' WHERE studentID = ? AND testID = ?';
        $args = [$studentID, $testID];
        return $this->fetchObject($query, $args, get_class($this));
    }

    public function getAllScoresBySubjectAndTest($testID, $subjectID)
    {
        $query = 'SELECT * FROM ' . self::TABLE . ' WHERE testID = ? AND subjectID = ?';
        $args = [$testID, $subjectID];
        return $this->fetchObject($query, $args, get_class($this));
    }

    public function getStudentsWithHighestScore()
    {
        $query = 'SELECT studentID, MAX(marks) AS highestScore FROM ' . self::TABLE .'GROUP BY studentID';
        return $this->fetchObject($query, '', get_class($this));
    }
    
}