<button type="button" class="btn btn-success" data-toggle="collapse" data-target="#allRecords">
         Click to see All Records <span class="caret"></span>
</button>

<div id="allRecords" class="collapse">
	<section class="">
		<table class="table table-striped table-inverse">
			<thead>
				<tr>
					<th>#</th>
					<th>Student Name</th>
					<th>Subject</th>
					<th>Test</th>
					<th>Score</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($scores as $score ) {
						?>
						<tr>
							<th scope="row"><?php echo $score->id; ?></th>
							<td><?php echo $student->getStudentName($score->studentID); ?></td>
							<td><?php echo $subject->getSubjectName($score->subjectID); ?></td>
							<td><?php echo $test->getTestName($score->testID); ?></td>
							<td><?php echo $score->marks; ?></td>
						</tr>
					<?php 
					}
				?>
			</tbody>
		</table>
	</section>
</div>
