<form class="form-horizontal">
	<div class="form-group">
		<label for="getSubjectName" class="control-label col-sm-4">Subject :</label>
		<div class="col-sm-5">
			<select class="form-control" id="subject" name="subject">
				<?php
				foreach ($subjects as $subject ) {
					?>
					<option id="<?php echo $subject->id; ?>"><?php echo $subject->name; ?></option>
					<?php 
				}
				?>
				
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="test" class="control-label col-sm-4">Test :</label>
		<div class="col-sm-5">
			<select class="form-control" id="test">
				<?php
				foreach ($tests as $test ) {
					?>
					<option id="<?php echo $test->id; ?>"><?php echo $test->name; ?></option>
					<?php 
				}
				?>
				
			</select>
		</div>
	</div>

	<div class="form-group">
		<section class=" col-sm-offset-1 col-sm-10 ">
			<table class="table table-striped table-inverse">
				<thead>
					<tr>
						<th>#</th>
						<th>Student Name</th>
						<th>Score</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($students as $student ) {
							?>
							<tr>
								<th scope="row"><?php echo $student->id; ?></th>
								<td><?php echo $student->name; ?></td>
								<td>
									<input type="number" class="form-control" pattern="[0-99]" min="1" max="100" required="true"> 
							</td>
						</tr>
					<?php 
					}?>
				</tbody>
			</table>
		</section>
	</div>

	<div class="form-group ">
		<button id="submit"  class="btn btn-primary col-sm-offset-4">Submit</button>
	</div>
</form>
