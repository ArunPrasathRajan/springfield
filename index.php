<?php

// print 'Hello universe!';
include_once 'Subject.php';
include_once 'Score.php';
include_once 'Test.php';
include_once 'Student.php';
include_once 'save.php';

$subject = new Subject();
$subjects = $subject->getSubjects();

$test = new Test();
$tests = $test->getTests();

$student = new Student();
$students = $student->getStudents();

$score = new Score();
$scores = $score->getScores();

// //var_dump($subjects);

// //var_dump($subject->getSubjectName(2));

// $score = new Score();
// //$score->insertScore(1,2,1,40);
// var_dump($score->getTotalMarksForStudentByTest(1,1));
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>SpringSMS</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
	<header>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">SpringSMS</a>
				</div>
			</div>
		</nav>
	</header>


	<div class="container">
		
		<section class="col-sm-7 pull-left" style="border:1px solid red;">
			<?php 
				include('views/createNewEntries.php'); 

				if(!empty($scores)){
			 		include('views/studentsTable.php'); 
				}
			 ?>
		</section>
		<section class="col-sm-5" id="result" >
			
			<div id="chartContainer" style="height: 400px;"></div>
		
				<div class="dropdown">
				  <button class="btn btn-primary dropdown-toggle btn-block  " type="button" data-toggle="dropdown">Students with the highest mark
				  <span class="caret"></span></button>
				  <ul class="dropdown-menu col-sm-12 ">
				    <li class="text-center"></li>
				  </ul>
				</div>
			</div>
		</section>
	</div>

	<footer style="margin-top: 20%;">
		<nav class="navbar  navbar-inverse">
		  <a class="navbar-brand" href="#">Fixed bottom</a>
		</nav>
	</footer>

	<script   src="https://code.jquery.com/jquery-3.2.1.min.js"   integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="   crossorigin="anonymous"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="canvasjs-1/canvasjs.min.js"></script>
	<script src="js/graph.js"></script>
	<script src="js/school.js"></script>

</body>
</html>
